*********
Sampo App
*********

.. image:: https://img.shields.io/badge/license-Public%20Domain-blue.svg
   :alt: Project license
   :target: https://unlicense.org

.. image:: https://img.shields.io/travis/sampoapp/sampo/master.svg
   :alt: Travis CI build status
   :target: https://travis-ci.org/sampoapp/sampo

|

https://sampoapp.org

----

Prerequisites
=============

Build Prerequisites
-------------------

- `Flutter <https://flutter.io/>`__

- `Android Studio <https://developer.android.com/studio/>`__
  (for Android only)

- `Xcode <https://developer.apple.com/xcode/>`__
  (for iOS only)

- `fastlane <https://fastlane.tools/>`__
  (optional)

Development Prerequisites
-------------------------

Development Environment
^^^^^^^^^^^^^^^^^^^^^^^

We recommend using `Visual Studio Code <https://code.visualstudio.com/>`__
as a comparatively lightweight development environment for developing this
app.

----

Installation
============

Build a debug release and run the app on an emulator or device as follows::

   $ flutter run

----

Development Tips
================

View the Localizations
----------------------

To view the string translations pretty-printed in the terminal, use
``column(1)`` (requires ``bsdmainutils`` on Debian and derivatives)::

   $ column -t -s $'\t' etc/strings.tsv

Dump the Database
-----------------

To dump the app's SQLite database using the `Android Debug Bridge
<https://developer.android.com/studio/command-line/adb>`__, execute::

   $ adb exec-out run-as org.sampoapp.android sqlite3 /data/data/org.sampoapp.android/databases/app.db .dump

To copy the app's SQLite database to your development host, execute::

   $ adb exec-out run-as org.sampoapp.android cat /data/data/org.sampoapp.android/databases/app.db > app.db

Share Link to App
-----------------

::

   $ adb shell am start -D -W -a android.intent.action.SEND -t text/plain -e android.intent.extra.TEXT https://example.org

See Also
========

- `Sampo Command-Line Interface (CLI) <https://github.com/sampoapp/sampo-cli>`__

- `Sampo Demo Scenarios <https://github.com/sampoapp/sampo-demos>`__
