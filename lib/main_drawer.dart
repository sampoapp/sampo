/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'src/config.dart';
import 'src/strings.dart';
import 'src/generated/classes.dart';

import 'config_screen.dart';

////////////////////////////////////////////////////////////////////////////////

class MainDrawer extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    final List<Widget> entityItems = EntityClasses.top.map<Widget>((modelID) {
      final EntityClass model = EntityClasses.all[modelID];
      return (config.getBool("class.${model.id}") == false) ? null : ListTile(
        leading: Icon(model.icon),
        title: Text(Strings.of(context).getPlural(model.id)),
        onTap: () => Navigator.of(context).pushNamed("/list/${model.id}"),
      );
    }).toList();

    final List<Widget> miscItems = <Widget>[
      Divider(),

      ListTile(
        leading: Icon(Icons.settings),
        title: Text(Strings.of(context).settings),
        onTap: () => Navigator.of(context).pushNamed(ConfigScreen.route),
      ),

      Divider(),

      ListTile(
        leading: Icon(Icons.report),
        title: Text(Strings.of(context).sendFeedback),
        onTap: () => launch(Strings.of(context).feedbackURL),
      ),

      AboutListTile(
        icon: FlutterLogo(), // TODO
        applicationVersion: Strings.of(context).appVersion,
        applicationIcon: FlutterLogo(), // TODO
        applicationLegalese: Strings.of(context).legalese,
        aboutBoxChildren: <Widget>[],
      ),
    ];

    final List<Widget> items = <Widget>[];
    items.addAll(entityItems.where((element) => element != null));
    items.addAll(miscItems.where((element) => element != null));

    return Drawer(
      child: ListView(
        primary: false,
        children: items,
      ),
    );
  }
}
