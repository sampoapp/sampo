/* This is free and unencumbered software released into the public domain. */

import 'dart:async' show Future;
import 'dart:ui' show Locale;

import 'package:flutter/foundation.dart' show SynchronousFuture;
import 'package:flutter/material.dart' show BuildContext, Localizations, LocalizationsDelegate;

import 'generated/strings.dart';

////////////////////////////////////////////////////////////////////////////////

class StringsDelegate extends LocalizationsDelegate<Strings> {
  final Set<String> supportedLanguageCodes;

  StringsDelegate() :
    supportedLanguageCodes = GeneratedStrings.supportedLocales.map((locale) => locale.languageCode).toSet();

  @override
  bool isSupported(final Locale locale) => supportedLanguageCodes.contains(locale.languageCode);

  @override
  Future<Strings> load(final Locale locale) {
    return SynchronousFuture<Strings>(Strings(locale));
  }

  @override
  bool shouldReload(final StringsDelegate _) => false;
}

////////////////////////////////////////////////////////////////////////////////

class Strings extends GeneratedStrings {
  static const defaultLocale    = Locale('en');
  static const supportedLocales = GeneratedStrings.supportedLocales;

  static Strings of(final BuildContext context) {
    return Localizations.of<Strings>(context, Strings);
  }

  Strings(final Locale currentLocale) : super(currentLocale, defaultLocale);

  String get todo => "TODO";

  String getSingular(final String id) => get(id);

  String getPlural(final String id) {
    if (id == 'person') return people; // FIXME
    return get(id) + "s"; // FIXME!
  }
}
