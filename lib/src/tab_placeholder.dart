/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';

////////////////////////////////////////////////////////////////////////////////

class TabPlaceholder extends StatelessWidget {
  final String title;

  TabPlaceholder({Key key, this.title}) : super(key: key);

  @override
  Widget build(final BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "TODO: " + title,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
