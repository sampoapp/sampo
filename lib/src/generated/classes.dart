/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart' show IconData, Icons;

////////////////////////////////////////////////////////////////////////////////

class EntityClass {
  final String id;
  final IconData icon;
  final String superclass;
  final List<String> subclasses;

  const EntityClass(this.id, {this.icon, this.superclass, this.subclasses});
}

////////////////////////////////////////////////////////////////////////////////

class EntityClasses {
  static const List<String> top = [
  // BEGIN TOP-LEVEL CLASSES
    "account",
    "bookmark",
    "contract",
    "document",
    "event",
    "network",
    "note",
    "paper",
    "payment",
    "person",
    "place",
    "project",
    "quote",
    "task",
  // END TOP-LEVEL CLASSES
  ];

  static const Map<String, EntityClass> all = {
  // BEGIN CLASS METADATA
    "account": EntityClass("account",
      icon: Icons.account_circle,
      superclass: null,
      subclasses: <String>[
        "bank_account",
      ],
    ),
    "bookmark": EntityClass("bookmark",
      icon: Icons.bookmark,
      superclass: null,
      subclasses: <String>[
        "amazon_product",
        "arto_note",
        "arxiv_article",
        "bitbucket_repository",
        "coursera_course",
        "dart_package",
        "facebook_page",
        "github_account",
        "github_repository",
        "google_document",
        "google_group",
        "google_play_app",
        "google_search",
        "hex_package",
        "ietf_rfc",
        "meetup_group",
        "python_package",
        "reddit_thread",
        "ruby_gem",
        "twitter_tweet",
        "vimeo_video",
        "website",
        "wikipedia_article",
        "wikipedia_category",
        "wikiquote_page",
        "wiktionary_entry",
        "youtube_channel",
        "youtube_playlist",
        "youtube_video",
      ],
    ),
    "contract": EntityClass("contract",
      icon: Icons.gavel,
      superclass: null,
      subclasses: <String>[],
    ),
    "document": EntityClass("document",
      icon: Icons.attachment,
      superclass: null,
      subclasses: <String>[
        "email",
      ],
    ),
    "event": EntityClass("event",
      icon: Icons.event,
      superclass: null,
      subclasses: <String>[
        "birthday",
        "holiday",
        "meeting",
        "meetup",
        "party",
      ],
    ),
    "network": EntityClass("network",
      icon: Icons.wifi,
      superclass: null,
      subclasses: <String>[
        "wired_network",
        "wireless_network",
      ],
    ),
    "note": EntityClass("note",
      icon: Icons.note,
      superclass: null,
      subclasses: <String>[],
    ),
    "paper": EntityClass("paper",
      icon: Icons.description,
      superclass: null,
      subclasses: <String>[],
    ),
    "payment": EntityClass("payment",
      icon: Icons.monetization_on,
      superclass: null,
      subclasses: <String>[],
    ),
    "person": EntityClass("person",
      icon: Icons.person,
      superclass: null,
      subclasses: <String>[
        "man",
        "woman",
      ],
    ),
    "place": EntityClass("place",
      icon: Icons.place,
      superclass: null,
      subclasses: <String>[
        "building",
        "monument",
        "restaurant",
        "venue",
      ],
    ),
    "project": EntityClass("project",
      icon: Icons.date_range,
      superclass: null,
      subclasses: <String>[],
    ),
    "quote": EntityClass("quote",
      icon: Icons.format_quote,
      superclass: null,
      subclasses: <String>[],
    ),
    "task": EntityClass("task",
      icon: Icons.assignment,
      superclass: null,
      subclasses: <String>[
        "assignment",
        "bounty",
        "chore",
        "errand",
        "promise",
        "purchase",
      ],
    ),
  // END CLASS METADATA
  };
}
