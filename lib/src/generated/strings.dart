/* This is free and unencumbered software released into the public domain. */

import 'dart:ui' show Locale;

abstract class GeneratedStrings {
  final Locale _currentLocale;
  final Locale _defaultLocale;

  GeneratedStrings(this._currentLocale, this._defaultLocale);

  String get(final String id) =>
    _data[id][_currentLocale.languageCode] ?? _data[id][_defaultLocale.languageCode];

  static const supportedLocales = <Locale>[
  // BEGIN LOCALES
    Locale('en'), // English
  // END LOCALES
  ];

  // BEGIN GETTERS
  String get account => get('account');
  String get accounts => get('accounts');
  String get active => get('active');
  String get addUser => get('addUser');
  String get all => get('all');
  String get amazon_product => get('amazon_product');
  String get appTitle => get('appTitle');
  String get appVersion => get('appVersion');
  String get archive => get('archive');
  String get archived => get('archived');
  String get arto_note => get('arto_note');
  String get arxiv_article => get('arxiv_article');
  String get assignment => get('assignment');
  String get backup => get('backup');
  String get bank_account => get('bank_account');
  String get birthday => get('birthday');
  String get birthdays => get('birthdays');
  String get bitbucket_repository => get('bitbucket_repository');
  String get book => get('book');
  String get bookmark => get('bookmark');
  String get bookmarks => get('bookmarks');
  String get books => get('books');
  String get bounties => get('bounties');
  String get bounty => get('bounty');
  String get boyfriend => get('boyfriend');
  String get building => get('building');
  String get cache => get('cache');
  String get cancel => get('cancel');
  String get chore => get('chore');
  String get clearDatabase => get('clearDatabase');
  String get clearDatabaseQ => get('clearDatabaseQ');
  String get colleague => get('colleague');
  String get colleagues => get('colleagues');
  String get confirmResetDatabase => get('confirmResetDatabase');
  String get contract => get('contract');
  String get contracts => get('contracts');
  String get coursera_course => get('coursera_course');
  String get create => get('create');
  String get credentials => get('credentials');
  String get dart_package => get('dart_package');
  String get database => get('database');
  String get diploma => get('diploma');
  String get diplomas => get('diplomas');
  String get document => get('document');
  String get documents => get('documents');
  String get dossier => get('dossier');
  String get dossiers => get('dossiers');
  String get email => get('email');
  String get emails => get('emails');
  String get enterSnapshotURL => get('enterSnapshotURL');
  String get entitites => get('entitites');
  String get entity => get('entity');
  String get errand => get('errand');
  String get error => get('error');
  String get event => get('event');
  String get events => get('events');
  String get facebook_page => get('facebook_page');
  String get family => get('family');
  String get favorite => get('favorite');
  String get favorites => get('favorites');
  String get feedbackURL => get('feedbackURL');
  String get girlfriend => get('girlfriend');
  String get github_account => get('github_account');
  String get github_repository => get('github_repository');
  String get google_document => get('google_document');
  String get google_group => get('google_group');
  String get google_play_app => get('google_play_app');
  String get google_search => get('google_search');
  String get hex_package => get('hex_package');
  String get history => get('history');
  String get holiday => get('holiday');
  String get household => get('household');
  String get husband => get('husband');
  String get idea => get('idea');
  String get ideas => get('ideas');
  String get ietf_rfc => get('ietf_rfc');
  String get inactive => get('inactive');
  String get key => get('key');
  String get keys => get('keys');
  String get legalese => get('legalese');
  String get license => get('license');
  String get licenses => get('licenses');
  String get load => get('load');
  String get loadDemo => get('loadDemo');
  String get loadDemoQ => get('loadDemoQ');
  String get loadSnapshot => get('loadSnapshot');
  String get man => get('man');
  String get meeting => get('meeting');
  String get meetup => get('meetup');
  String get meetup_group => get('meetup_group');
  String get membership => get('membership');
  String get memberships => get('memberships');
  String get memories => get('memories');
  String get memory => get('memory');
  String get monument => get('monument');
  String get movie => get('movie');
  String get movies => get('movies');
  String get nearby => get('nearby');
  String get network => get('network');
  String get networks => get('networks');
  String get note => get('note');
  String get notes => get('notes');
  String get overdue => get('overdue');
  String get paper => get('paper');
  String get papers => get('papers');
  String get partner => get('partner');
  String get party => get('party');
  String get payment => get('payment');
  String get payments => get('payments');
  String get people => get('people');
  String get person => get('person');
  String get photo => get('photo');
  String get photos => get('photos');
  String get place => get('place');
  String get places => get('places');
  String get proceed => get('proceed');
  String get project => get('project');
  String get projects => get('projects');
  String get promise => get('promise');
  String get purchase => get('purchase');
  String get purchases => get('purchases');
  String get python_package => get('python_package');
  String get quote => get('quote');
  String get quotes => get('quotes');
  String get read => get('read');
  String get receipt => get('receipt');
  String get receipts => get('receipts');
  String get recent => get('recent');
  String get reddit_thread => get('reddit_thread');
  String get restaurant => get('restaurant');
  String get ruby_gem => get('ruby_gem');
  String get save => get('save');
  String get saveSnapshot => get('saveSnapshot');
  String get sendFeedback => get('sendFeedback');
  String get server => get('server');
  String get servers => get('servers');
  String get settings => get('settings');
  String get starred => get('starred');
  String get task => get('task');
  String get tasks => get('tasks');
  String get today => get('today');
  String get tomorrow => get('tomorrow');
  String get twitter_tweet => get('twitter_tweet');
  String get unread => get('unread');
  String get upcoming => get('upcoming');
  String get user => get('user');
  String get users => get('users');
  String get venue => get('venue');
  String get vimeo_video => get('vimeo_video');
  String get website => get('website');
  String get wife => get('wife');
  String get wikipedia_article => get('wikipedia_article');
  String get wikipedia_category => get('wikipedia_category');
  String get wikiquote_page => get('wikiquote_page');
  String get wiktionary_entry => get('wiktionary_entry');
  String get wired_network => get('wired_network');
  String get wireless_network => get('wireless_network');
  String get woman => get('woman');
  String get yesterday => get('yesterday');
  String get youtube_channel => get('youtube_channel');
  String get youtube_playlist => get('youtube_playlist');
  String get youtube_video => get('youtube_video');
  // END GETTERS

  static Map<String, Map<String, String>> _data = {
  // BEGIN STRINGS
    'account': {
      'en': "Account",
    },
    'accounts': {
      'en': "Accounts",
    },
    'active': {
      'en': "Active",
    },
    'addUser': {
      'en': "Add user",
    },
    'all': {
      'en': "All",
    },
    'amazon_product': {
      'en': "Amazon product page",
    },
    'appTitle': {
      'en': "Sampo",
    },
    'appVersion': {
      'en': "January 2019",
    },
    'archive': {
      'en': "Archive",
    },
    'archived': {
      'en': "Archived",
    },
    'arto_note': {
      'en': "Arto's note",
    },
    'arxiv_article': {
      'en': "arXiv article",
    },
    'assignment': {
      'en': "Assignment",
    },
    'backup': {
      'en': "Backup",
    },
    'bank_account': {
      'en': "Bank account",
    },
    'birthday': {
      'en': "Birthday",
    },
    'birthdays': {
      'en': "Birthdays",
    },
    'bitbucket_repository': {
      'en': "Bitbucket repository",
    },
    'book': {
      'en': "Book",
    },
    'bookmark': {
      'en': "Bookmark",
    },
    'bookmarks': {
      'en': "Bookmarks",
    },
    'books': {
      'en': "Books",
    },
    'bounties': {
      'en': "Bounties",
    },
    'bounty': {
      'en': "Bounty",
    },
    'boyfriend': {
      'en': "Boyfriend",
    },
    'building': {
      'en': "Building",
    },
    'cache': {
      'en': "Cache",
    },
    'cancel': {
      'en': "Cancel",
    },
    'chore': {
      'en': "Chore",
    },
    'clearDatabase': {
      'en': "Clear database",
    },
    'clearDatabaseQ': {
      'en': "Clear database?",
    },
    'colleague': {
      'en': "Colleague",
    },
    'colleagues': {
      'en': "Colleagues",
    },
    'confirmResetDatabase': {
      'en': "This will reset the database and delete your data.",
    },
    'contract': {
      'en': "Contract",
    },
    'contracts': {
      'en': "Contracts",
    },
    'coursera_course': {
      'en': "Coursera course description",
    },
    'create': {
      'en': "Create",
    },
    'credentials': {
      'en': "Credentials",
    },
    'dart_package': {
      'en': "Dart/Flutter package",
    },
    'database': {
      'en': "Database",
    },
    'diploma': {
      'en': "Diploma",
    },
    'diplomas': {
      'en': "Diplomas",
    },
    'document': {
      'en': "Document",
    },
    'documents': {
      'en': "Documents",
    },
    'dossier': {
      'en': "Dossier",
    },
    'dossiers': {
      'en': "Dossiers",
    },
    'email': {
      'en': "Email",
    },
    'emails': {
      'en': "Emails",
    },
    'enterSnapshotURL': {
      'en': "Enter the snapshot URL:",
    },
    'entitites': {
      'en': "Entitites",
    },
    'entity': {
      'en': "Entity",
    },
    'errand': {
      'en': "Errand",
    },
    'error': {
      'en': "Error",
    },
    'event': {
      'en': "Event",
    },
    'events': {
      'en': "Events",
    },
    'facebook_page': {
      'en': "Facebook page",
    },
    'family': {
      'en': "Family",
    },
    'favorite': {
      'en': "Favorite",
    },
    'favorites': {
      'en': "Favorites",
    },
    'feedbackURL': {
      'en': "https://github.com/sampoapp/sampo/issues/new",
    },
    'girlfriend': {
      'en': "Girlfriend",
    },
    'github_account': {
      'en': "GitHub account",
    },
    'github_repository': {
      'en': "GitHub repository",
    },
    'google_document': {
      'en': "Google Docs/Sheets/Slides document",
    },
    'google_group': {
      'en': "Google Groups forum",
    },
    'google_play_app': {
      'en': "Google Play app",
    },
    'google_search': {
      'en': "Google search result",
    },
    'hex_package': {
      'en': "Hex package",
    },
    'history': {
      'en': "History",
    },
    'holiday': {
      'en': "Holiday",
    },
    'household': {
      'en': "Household",
    },
    'husband': {
      'en': "Husband",
    },
    'idea': {
      'en': "Idea",
    },
    'ideas': {
      'en': "Ideas",
    },
    'ietf_rfc': {
      'en': "IETF RFC memorandum",
    },
    'inactive': {
      'en': "Inactive",
    },
    'key': {
      'en': "Key",
    },
    'keys': {
      'en': "Keys",
    },
    'legalese': {
      'en': "This is free and unencumbered software released into the public domain.",
    },
    'license': {
      'en': "License",
    },
    'licenses': {
      'en': "Licenses",
    },
    'load': {
      'en': "Load",
    },
    'loadDemo': {
      'en': "Load demo",
    },
    'loadDemoQ': {
      'en': "Load demo data?",
    },
    'loadSnapshot': {
      'en': "Load snapshot",
    },
    'man': {
      'en': "Man",
    },
    'meeting': {
      'en': "Meeting",
    },
    'meetup': {
      'en': "Meetup",
    },
    'meetup_group': {
      'en': "Meetup group",
    },
    'membership': {
      'en': "Membership",
    },
    'memberships': {
      'en': "Memberships",
    },
    'memories': {
      'en': "Memories",
    },
    'memory': {
      'en': "Memory",
    },
    'monument': {
      'en': "Monument",
    },
    'movie': {
      'en': "Movie",
    },
    'movies': {
      'en': "Movies",
    },
    'nearby': {
      'en': "Nearby",
    },
    'network': {
      'en': "Network",
    },
    'networks': {
      'en': "Networks",
    },
    'note': {
      'en': "Note",
    },
    'notes': {
      'en': "Notes",
    },
    'overdue': {
      'en': "Overdue",
    },
    'paper': {
      'en': "Paper",
    },
    'papers': {
      'en': "Papers",
    },
    'partner': {
      'en': "Partner",
    },
    'party': {
      'en': "Party",
    },
    'payment': {
      'en': "Payment",
    },
    'payments': {
      'en': "Payments",
    },
    'people': {
      'en': "People",
    },
    'person': {
      'en': "Person",
    },
    'photo': {
      'en': "Photo",
    },
    'photos': {
      'en': "Photos",
    },
    'place': {
      'en': "Place",
    },
    'places': {
      'en': "Places",
    },
    'proceed': {
      'en': "Proceed",
    },
    'project': {
      'en': "Project",
    },
    'projects': {
      'en': "Projects",
    },
    'promise': {
      'en': "Promise",
    },
    'purchase': {
      'en': "Purchase",
    },
    'purchases': {
      'en': "Purchases",
    },
    'python_package': {
      'en': "PyPI package",
    },
    'quote': {
      'en': "Quote",
    },
    'quotes': {
      'en': "Quotes",
    },
    'read': {
      'en': "Read",
    },
    'receipt': {
      'en': "Receipt",
    },
    'receipts': {
      'en': "Receipts",
    },
    'recent': {
      'en': "Recent",
    },
    'reddit_thread': {
      'en': "Reddit thread",
    },
    'restaurant': {
      'en': "Restaurant",
    },
    'ruby_gem': {
      'en': "RubyGems gem",
    },
    'save': {
      'en': "Save",
    },
    'saveSnapshot': {
      'en': "Save snapshot",
    },
    'sendFeedback': {
      'en': "Send feedback",
    },
    'server': {
      'en': "Server",
    },
    'servers': {
      'en': "Servers",
    },
    'settings': {
      'en': "Settings",
    },
    'starred': {
      'en': "Starred",
    },
    'task': {
      'en': "Task",
    },
    'tasks': {
      'en': "Tasks",
    },
    'today': {
      'en': "Today",
    },
    'tomorrow': {
      'en': "Tomorrow",
    },
    'twitter_tweet': {
      'en': "Twitter tweet",
    },
    'unread': {
      'en': "Unread",
    },
    'upcoming': {
      'en': "Upcoming",
    },
    'user': {
      'en': "User",
    },
    'users': {
      'en': "Users",
    },
    'venue': {
      'en': "Venue",
    },
    'vimeo_video': {
      'en': "Vimeo video",
    },
    'website': {
      'en': "Website",
    },
    'wife': {
      'en': "Wife",
    },
    'wikipedia_article': {
      'en': "Wikipedia article",
    },
    'wikipedia_category': {
      'en': "Wikipedia category",
    },
    'wikiquote_page': {
      'en': "Wikiquote page",
    },
    'wiktionary_entry': {
      'en': "Wiktionary entry",
    },
    'wired_network': {
      'en': "Wired network",
    },
    'wireless_network': {
      'en': "Wireless network",
    },
    'woman': {
      'en': "Woman",
    },
    'yesterday': {
      'en': "Yesterday",
    },
    'youtube_channel': {
      'en': "YouTube channel",
    },
    'youtube_playlist': {
      'en': "YouTube playlist",
    },
    'youtube_video': {
      'en': "YouTube video",
    },
  // END STRINGS
  };
}
