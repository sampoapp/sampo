/* This is free and unencumbered software released into the public domain. */

import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

////////////////////////////////////////////////////////////////////////////////

final Config config = Config();

////////////////////////////////////////////////////////////////////////////////

class Config {
  SharedPreferences _prefs;

  Future<void> init() async {
    _prefs = await SharedPreferences.getInstance();
  }

  Future<bool> clear() => _prefs.clear();

  bool hasKey(final String key) => (_prefs != null && _prefs.get(key) != null);

  bool getBool(final String key) => _prefs.getBool(key);

  String getString(final String key) => _prefs.getString(key);

  Future<bool> setBool(final String key, final bool value) => _prefs.setBool(key, value);

  Future<bool> setString(final String key, final String value) => _prefs.setString(key, value);
}
