/* This is free and unencumbered software released into the public domain. */

import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/services.dart' show rootBundle;
import 'package:path/path.dart' as path;
import 'package:sqflite/sqflite.dart' as sqflite;

import 'query_result.dart';

////////////////////////////////////////////////////////////////////////////////

final Database database = Database();

////////////////////////////////////////////////////////////////////////////////

class Database {
  sqflite.Database db;

  Future<String> get dirpath => sqflite.getDatabasesPath();

  Future<String> get filepath async {
    return path.join(await dirpath, "app.db");
  }

  Future<void> init() async {
    await sqflite.Sqflite.devSetDebugModeOn(true); // DEBUG
    //await sqflite.deleteDatabase(await filepath); // DEBUG
    await open();
  }

  Future<void> open() async {
    final String dbPath = await filepath;
    db = await sqflite.openDatabase(dbPath,
      version: 1,
      onConfigure: _configure,
      onCreate: (final sqflite.Database db, final int version) async {
        assert(version == 1);
        await _clear(db);
      },
      onUpgrade: (final sqflite.Database db, final int oldVersion, final int newVersion) async {
        // See: https://www.sqlite.org/pragma.html#pragma_user_version
        assert(newVersion > oldVersion);
        // TODO
      },
    );
  }

  Future<void> _configure(final sqflite.Database db) async {
    // See: https://www.sqlite.org/pragma.html#pragma_journal_mode
    await db.rawQuery('PRAGMA journal_mode=DELETE');
    // See: https://www.sqlite.org/pragma.html#pragma_encoding
    await db.execute('PRAGMA encoding="UTF-8"');
    // See: https://www.sqlite.org/pragma.html#pragma_foreign_keys
    await db.execute('PRAGMA foreign_keys=ON');
  }

  Future<void> _clear(final sqflite.Database db) async {
    final String sqlSchema = await rootBundle.loadString('etc/schema.sql');
    for (var sql in sqlSchema.split(";\n")) {
      if (sql.isNotEmpty) {
        await db.execute(sql);
      }
    }
  }

  Future<void> close() async {
    await db?.close();
    db = null;
  }

  Future<void> clear() async {
    await db.execute('PRAGMA foreign_keys=OFF');
    await _clear(db);
    await db.execute('PRAGMA foreign_keys=ON');
  }

  Future<void> replaceWithFile(final File file) async {
    final String dbPath = await filepath;
    await close();
    await sqflite.deleteDatabase(dbPath);
    await file.copy(dbPath);
    await open();
  }

  Future<void> replaceWithData(final ByteBuffer data) async {
    final String dbPath = await filepath;
    await close();
    await sqflite.deleteDatabase(dbPath);
    await File(dbPath).writeAsBytes(data.asUint8List(), flush: true);
    await open();
  }

  Future<QueryResult> findAccounts({bool starred}) {
    return db.query("data_account",
      orderBy: "id DESC",
      limit: (starred == true ? 0 : null), // TODO
    )
    .then((rows) => QueryResult(rows));
  }

  Future<QueryResult> findBookmarks({bool starred}) {
    return db.query("data_bookmark",
      orderBy: "id DESC",
      limit: (starred == true ? 0 : null), // TODO
    )
    .then((rows) => QueryResult(rows));
  }

  Future<QueryResult> findContracts({bool starred}) {
    return db.query("data_contract",
      orderBy: "id DESC",
      limit: (starred == true ? 0 : null), // TODO
    )
    .then((rows) => QueryResult(rows));
  }

  Future<QueryResult> findDocuments({bool starred}) {
    return db.query("data_document",
      orderBy: "id DESC",
      limit: (starred == true ? 0 : null), // TODO
    )
    .then((rows) => QueryResult(rows));
  }

  Future<QueryResult> findEventsBeforeNow() {
    return db.query("data_event",
      where: "date < ?",
      whereArgs: [DateTime.now().toIso8601String().substring(0, 10)],
      orderBy: "date DESC",
    )
    .then((rows) => QueryResult(rows));
  }

  Future<QueryResult> findEventsAfterNow() {
    return db.query("data_event",
      where: "date >= ?",
      whereArgs: [DateTime.now().toIso8601String().substring(0, 10)],
      orderBy: "date ASC",
    )
    .then((rows) => QueryResult(rows));
  }

  Future<QueryResult> findNotes() {
    return db.query("data_note",
      orderBy: "id DESC",
    )
    .then((rows) => QueryResult(rows));
  }

  Future<QueryResult> findPapers({bool starred, bool read}) {
    return db.query("data_paper",
      orderBy: "id DESC",
      limit: (starred == true || read == true ? 0 : null), // TODO
    )
    .then((rows) => QueryResult(rows));
  }

  Future<QueryResult> findPayments() {
    return db.query("data_payment",
      orderBy: "due_at ASC, id DESC",
    )
    .then((rows) => QueryResult(rows));
  }

  Future<QueryResult> findPeople({bool starred}) {
    return db.query("data_person",
      orderBy: "id DESC",
      limit: (starred == true ? 0 : null), // TODO
    )
    .then((rows) => QueryResult(rows));
  }

  Future<QueryResult> findPlaces() {
    return db.query("data_place",
      orderBy: "id DESC",
    )
    .then((rows) => QueryResult(rows));
  }

  Future<QueryResult> findProjects() {
    return db.query("data_project",
      orderBy: "id DESC",
    )
    .then((rows) => QueryResult(rows));
  }

  Future<QueryResult> findQuotes({bool starred}) {
    return db.query("data_quote",
      orderBy: "id DESC",
      limit: (starred == true ? 0 : null), // TODO
    )
    .then((rows) => QueryResult(rows));
  }
}
