/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';

import 'strings.dart';
import 'tab_label.dart';

////////////////////////////////////////////////////////////////////////////////

class EntityScreen extends StatelessWidget {
  final String title;
  final List<String> subclasses;
  final Map<String, Widget> tabs;
  final VoidCallback onPressCreate;

  EntityScreen({Key key, this.title, this.subclasses, this.tabs, this.onPressCreate}) : super(key: key);

  @override
  Widget build(final BuildContext context) {
    return DefaultTabController(
      length: tabs.length,
      child: Scaffold(
        appBar: AppBar(
          title: Text(title),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search),
              onPressed: null, // TODO
            ),
            IconButton(
              icon: Icon(Icons.filter_list),
              tooltip: "Show filters", // TODO
              onPressed: subclasses.isEmpty ? null : () => _showFilterSheet(context),
            ),
            IconButton(
              icon: Icon(Icons.sort_by_alpha),
              onPressed: null, // TODO
            ),
            //IconButton(icon: Icon(Icons.more_vert), onPressed: () => {}),
          ],
          bottom: TabBar(
            tabs: tabs.keys.map((s) => Tab(child: TabLabel((context) => s))).toList(),
          ),
        ),
        body: TabBarView(
          children: tabs.values.toList(),
        ),
        floatingActionButton: true ? null : FloatingActionButton( // TODO
          tooltip: Strings.of(context).create,
          child: Icon(Icons.add),
          onPressed: onPressCreate,
        ),
      ),
    );
  }

  void _showFilterSheet(final BuildContext context) {
    showModalBottomSheet<void>(context: context, builder: (final BuildContext context) {
      return Container(
        decoration: BoxDecoration(
          border: Border(top: BorderSide(color: Colors.white30)),
        ),
        child: ListView(
          shrinkWrap: true,
          primary: false,
          children: subclasses.map((subclass) { // TODO: sort by title
            return MergeSemantics(
              child: ListTile(
                dense: true,
                title: Text(Strings.of(context).get(subclass)),
                trailing: Checkbox(
                  value: true, // TODO
                  onChanged: null, // TODO
                ),
              ),
            );
          }).toList(),
        ),
      );
    });
  }
}
