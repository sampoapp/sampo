/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'error_report.dart';
import 'query_result.dart';

////////////////////////////////////////////////////////////////////////////////

typedef Widget QueryCallback(BuildContext context, QueryResult rows);

////////////////////////////////////////////////////////////////////////////////

class QueryBuilder extends StatefulWidget {
  final Future<QueryResult> future;
  final QueryCallback onSuccess;

  const QueryBuilder({
    Key key,
    @required this.future,
    @required this.onSuccess
  }) : assert(future != null),
       assert(onSuccess != null),
       super(key: key);

  @override
  State<QueryBuilder> createState() => _QueryState();
}

////////////////////////////////////////////////////////////////////////////////

class _QueryState extends State<QueryBuilder> {
  Object _activeCallbackIdentity;
  AsyncSnapshot<QueryResult> _snapshot;

  @override
  void initState() {
    super.initState();
    _snapshot = AsyncSnapshot<QueryResult>.withData(ConnectionState.none, null);
    _subscribe();
  }

  @override
  void didUpdateWidget(final QueryBuilder oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.future != widget.future) {
      if (_activeCallbackIdentity != null) {
        _unsubscribe();
        _snapshot = _snapshot.inState(ConnectionState.none);
      }
      _subscribe();
    }
  }

  @override
  Widget build(final BuildContext context) {
    switch (_snapshot.connectionState) {
      case ConnectionState.none:
      case ConnectionState.active:
      case ConnectionState.waiting:
        return Center(
          child: SpinKitFadingCircle(
            color: Colors.grey,
            size: 300.0,
          ),
        );
      case ConnectionState.done:
        if (_snapshot.hasError) {
          return ErrorReport(error: _snapshot.error);
        }
        return widget.onSuccess(context, _snapshot.data);
    }
    assert(false, "unreachable");
    return null; // unreachable
  }

  @override
  void dispose() {
    _unsubscribe();
    super.dispose();
  }

  void _subscribe() {
    if (widget.future != null) {
      final Object callbackIdentity = Object();
      _activeCallbackIdentity = callbackIdentity;
      widget.future.then<void>((final QueryResult data) {
        if (_activeCallbackIdentity == callbackIdentity) {
          setState(() {
            _snapshot = AsyncSnapshot<QueryResult>.withData(ConnectionState.done, data);
          });
        }
      },
      onError: (final Object error) {
        if (_activeCallbackIdentity == callbackIdentity) {
          setState(() {
            _snapshot = AsyncSnapshot<QueryResult>.withError(ConnectionState.done, error);
          });
        }
      });
      _snapshot = _snapshot.inState(ConnectionState.waiting);
    }
  }

  void _unsubscribe() {
    _activeCallbackIdentity = null;
  }
}
