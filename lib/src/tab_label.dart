/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';

////////////////////////////////////////////////////////////////////////////////

typedef String TabLabelCallback(BuildContext context);

////////////////////////////////////////////////////////////////////////////////

class TabLabel extends StatelessWidget {
  TabLabel(this.onGenerateTitle, {Key key}) : super(key: key);

  final TabLabelCallback onGenerateTitle;

  @override
  Widget build(final BuildContext context) {
    return Text(onGenerateTitle(context).toUpperCase(),
      softWrap: false,
      overflow: TextOverflow.fade,
    );
  }
}
