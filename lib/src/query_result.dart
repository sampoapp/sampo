/* This is free and unencumbered software released into the public domain. */

////////////////////////////////////////////////////////////////////////////////

class QueryResult {
  final List<Map<String, dynamic>> rows;

  QueryResult(this.rows) : assert(rows != null);

  int get length => rows.length;
}
