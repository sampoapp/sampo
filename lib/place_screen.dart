/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';

import 'src/database.dart';
import 'src/entity_screen.dart';
import 'src/query_builder.dart';
import 'src/query_result.dart';
import 'src/strings.dart';
import 'src/tab_placeholder.dart';
import 'src/generated/classes.dart';

import 'place_form.dart';

////////////////////////////////////////////////////////////////////////////////

class PlaceScreen extends StatefulWidget {
  @override
  State<PlaceScreen> createState() => _PlaceState();
}

////////////////////////////////////////////////////////////////////////////////

class _PlaceState extends State<PlaceScreen> {
  @override
  Widget build(final BuildContext context) {
    return EntityScreen(
      title: Strings.of(context).places,
      subclasses: EntityClasses.all['place'].subclasses,
      tabs: {
        Strings.of(context).nearby: PlacesNearbyTab(),
        Strings.of(context).recent: PlacesRecentTab(),
        Strings.of(context).all:    PlacesAllTab(),
      },
      onPressCreate: () {
        Navigator.push(context, MaterialPageRoute<void>(
          builder: (final BuildContext context) => PlaceForm(),
          fullscreenDialog: true,
        ));
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class PlacesAllTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return QueryBuilder(
      future: database.findPlaces(),
      onSuccess: (final BuildContext context, final QueryResult result) {
        return ListView.builder(
          padding: EdgeInsets.all(8.0),
          itemCount: result.length,
          itemBuilder: (final BuildContext context, final int index) {
            var row = result.rows[index];
            return ListTile(
              title: Text("${row['title']}"), // TODO
            );
          },
        );
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class PlacesNearbyTab extends TabPlaceholder {
  PlacesNearbyTab({Key key}) : super(title: "Nearby places", key: key); // TODO
}

////////////////////////////////////////////////////////////////////////////////

class PlacesRecentTab extends TabPlaceholder {
  PlacesRecentTab({Key key}) : super(title: "Recent places", key: key); // TODO
}
