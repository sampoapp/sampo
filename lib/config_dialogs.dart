/* This is free and unencumbered software released into the public domain. */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'src/strings.dart';

////////////////////////////////////////////////////////////////////////////////

class LoadSnapshotCanceled implements Exception {}

////////////////////////////////////////////////////////////////////////////////

class LoadSnapshotDialog extends StatefulWidget {
  LoadSnapshotDialog({Key key, this.defaultURL}) : super(key: key);

  final String defaultURL;

  static Future<Uri> show(final BuildContext context, final String defaultURL) async {
    return showDialog<Uri>(
      context: context,
      barrierDismissible: true,
      builder: (final BuildContext context) => LoadSnapshotDialog(defaultURL: defaultURL)
    );
  }

  @override
  State<LoadSnapshotDialog> createState() => _LoadSnapshotState();
}

////////////////////////////////////////////////////////////////////////////////

class _LoadSnapshotState extends State<LoadSnapshotDialog> {
  Uri _url;
  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    _url = (widget.defaultURL != null) ? Uri.tryParse(widget.defaultURL) : null;
    _controller.text = widget.defaultURL;
    _controller.addListener(_onChanged);
  }

  @override
  void dispose() {
    _controller.removeListener(_onChanged);
    _controller.dispose();
    super.dispose();
  }

  void _onChanged() {
    final String value = _controller.text;
    setState(() {
      _url = null;
      if (value == null || value.trim().isEmpty) return;
      final Uri uri = Uri.tryParse(value);
      if (uri == null) return;
      if (uri.path.isEmpty || uri.path == '/') return;
      if (uri.scheme != 'file' && uri.scheme != 'http' && uri.scheme != 'https') return;
      _url = uri;
    });
  }

  @override
  Widget build(final BuildContext context) {
    return AlertDialog(
      title: Text(Strings.of(context).loadSnapshot),
      content: TextField(
        decoration: InputDecoration(
          labelText: Strings.of(context).enterSnapshotURL,
        ),
        controller: _controller,
        maxLength: 255,
        keyboardType: TextInputType.url,
        textInputAction: TextInputAction.go,
        autofocus: true,
        autocorrect: false,
      ),
      actions: <Widget>[
        FlatButton(
          child: Text(Strings.of(context).cancel.toUpperCase()),
          onPressed: () => Navigator.of(context).pop(null),
        ),
        FlatButton(
          child: Text(Strings.of(context).loadSnapshot.toUpperCase()),
          onPressed: (_url == null) ? null : () => Navigator.of(context).pop(_url),
        ),
      ],
    );
  }
}
