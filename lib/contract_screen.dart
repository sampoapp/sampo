/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';

import 'src/database.dart';
import 'src/entity_screen.dart';
import 'src/query_builder.dart';
import 'src/query_result.dart';
import 'src/strings.dart';
import 'src/generated/classes.dart';

import 'contract_form.dart';
import 'contract_view.dart';

////////////////////////////////////////////////////////////////////////////////

class ContractScreen extends StatefulWidget {
  @override
  State<ContractScreen> createState() => _ContractState();
}

////////////////////////////////////////////////////////////////////////////////

class _ContractState extends State<ContractScreen> {
  @override
  Widget build(final BuildContext context) {
    return EntityScreen(
      title: Strings.of(context).contracts,
      subclasses: EntityClasses.all['contract'].subclasses,
      tabs: {
        Strings.of(context).starred: ContractStarredTab(),
        Strings.of(context).all:     ContractAllTab(),
      },
      onPressCreate: () {
        Navigator.push(context, MaterialPageRoute<void>(
          builder: (final BuildContext context) => ContractForm(),
          fullscreenDialog: true,
        ));
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class ContractStarredTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return QueryBuilder(
      future: database.findContracts(starred: true),
      onSuccess: (final BuildContext context, final QueryResult result) {
        return ContractView(result);
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class ContractAllTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return QueryBuilder(
      future: database.findContracts(),
      onSuccess: (final BuildContext context, final QueryResult result) {
        return ContractView(result);
      },
    );
  }
}
