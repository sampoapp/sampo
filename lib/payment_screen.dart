/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';

import 'src/database.dart';
import 'src/entity_screen.dart';
import 'src/query_builder.dart';
import 'src/query_result.dart';
import 'src/strings.dart';
import 'src/tab_placeholder.dart';
import 'src/generated/classes.dart';

import 'payment_form.dart';

////////////////////////////////////////////////////////////////////////////////

class PaymentScreen extends StatefulWidget {
  @override
  State<PaymentScreen> createState() => _PaymentState();
}

////////////////////////////////////////////////////////////////////////////////

class _PaymentState extends State<PaymentScreen> {
  @override
  Widget build(final BuildContext context) {
    return EntityScreen(
      title: Strings.of(context).payments,
      subclasses: EntityClasses.all['payment'].subclasses,
      tabs: {
        Strings.of(context).upcoming: PaymentsUpcomingTab(),
        Strings.of(context).overdue:  PaymentsOverdueTab(),
      },
      onPressCreate: () {
        Navigator.push(context, MaterialPageRoute<void>(
          builder: (final BuildContext context) => PaymentForm(),
          fullscreenDialog: true,
        ));
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class PaymentsOverdueTab extends TabPlaceholder {
  PaymentsOverdueTab({Key key}) : super(title: "Overdue payments", key: key); // TODO
}

////////////////////////////////////////////////////////////////////////////////

class PaymentsUpcomingTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return QueryBuilder(
      future: database.findPayments(), // TODO
      onSuccess: (final BuildContext context, final QueryResult result) {
        return ListView.builder(
          padding: EdgeInsets.all(8.0),
          itemCount: result.length,
          itemBuilder: (final BuildContext context, final int index) {
            var row = result.rows[index];
            return ListTile(
              title: Text("${row['title']}"), // TODO
            );
          },
        );
      },
    );
  }
}
