/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';

import 'src/database.dart';
import 'src/entity_screen.dart';
import 'src/query_builder.dart';
import 'src/query_result.dart';
import 'src/strings.dart';
import 'src/generated/classes.dart';

import 'note_form.dart';
import 'note_view.dart';

////////////////////////////////////////////////////////////////////////////////

class NoteScreen extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Strings.of(context).notes),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.search), onPressed: null), // TODO
          IconButton(icon: Icon(Icons.filter_list), onPressed: null), // TODO
          IconButton(icon: Icon(Icons.sort_by_alpha), onPressed: null), // TODO
          //IconButton(icon: Icon(Icons.more_vert), onPressed: null),
        ],
      ),
      body: QueryBuilder(
        future: database.findNotes(),
        onSuccess: (final BuildContext context, final QueryResult result) {
          return NoteView(result);
        },
      ),
      floatingActionButton: true ? null : FloatingActionButton( // TODO
        tooltip: Strings.of(context).create,
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.push(context, MaterialPageRoute<void>(
            builder: (final BuildContext context) => NoteForm(),
            fullscreenDialog: true,
          ));
        },
      ),
    );
  }
}
