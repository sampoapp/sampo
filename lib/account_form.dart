/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';

import 'src/strings.dart';

////////////////////////////////////////////////////////////////////////////////

class AccountForm extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Add account"), // TODO
        actions: <Widget>[
          FlatButton(
            child: Text(Strings.of(context).save.toUpperCase()),
            onPressed: null, // TODO: () { Navigator.pop(context, null); }
          )
        ],
      ),
      body: Form(
        child: ListView(
          // TODO
        ),
      ),
    );
  }
}
