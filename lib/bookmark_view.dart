/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'src/query_result.dart';

////////////////////////////////////////////////////////////////////////////////

class BookmarkView extends StatelessWidget {
  final QueryResult result;

  BookmarkView(this.result, {Key key}) : super(key: key);

  @override
  Widget build(final BuildContext context) {
    return ListView.separated(
      padding: EdgeInsets.all(8.0),
      itemCount: result.length,
      itemBuilder: (final BuildContext context, final int index) {
        var row = result.rows[index];
        return GestureDetector(
          onTap: () async {
            await launch(row['url']);
          },
          child: ListTile(
            leading: Icon(Icons.bookmark),
            title: Text(row['title']),
            subtitle: Text(row['url']),
            trailing: Icon(Icons.info, color: Theme.of(context).disabledColor),
          ),
        );
      },
      separatorBuilder: (final BuildContext context, final int index) {
        return Divider();
      },
    );
  }
}
