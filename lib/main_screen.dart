/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';

import 'src/config.dart';
import 'src/strings.dart';
import 'src/tab_label.dart';

import 'main_drawer.dart';

////////////////////////////////////////////////////////////////////////////////

class MainScreen extends StatelessWidget {
  static const route = "/";

  final List<Tab> _tabs = <Tab>[
    Tab(child: TabLabel((context) => Strings.of(context).today)),
    Tab(child: TabLabel((context) => Strings.of(context).tomorrow)),
  ];

  MainScreen({Key key}) : super(key: key);

  @override
  Widget build(final BuildContext context) {
    return DefaultTabController(
      length: _tabs.length,
      child: Scaffold(
        appBar: AppBar(
          title: Text(Strings.of(context).appTitle),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.sync),
              onPressed: !config.hasKey('config.snapshotURL') ? null : () {},
            ),
            IconButton(
              icon: Icon(Icons.more_vert),
              onPressed: null,
            ),
          ].where((element) => element != null).toList(),
          bottom: TabBar(tabs: _tabs),
        ),
        drawer: MainDrawer(),
        body: TabBarView(
          children: <Widget>[
            MainTodayTab(),
            MainTomorrowTab(),
          ],
        ),
      ),
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class MainTodayTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return ListView.separated(
      padding: EdgeInsets.all(8.0),
      itemCount: 0,
      itemBuilder: (final BuildContext context, final int index) {
        return null; // TODO
      },
      separatorBuilder: (final BuildContext context, final int index) {
        return Divider();
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class MainTomorrowTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return ListView.separated(
      padding: EdgeInsets.all(8.0),
      itemCount: 0,
      itemBuilder: (final BuildContext context, final int index) {
        return null; // TODO
      },
      separatorBuilder: (final BuildContext context, final int index) {
        return Divider();
      },
    );
  }
}
