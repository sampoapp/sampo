/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';

import 'src/database.dart';
import 'src/entity_screen.dart';
import 'src/query_builder.dart';
import 'src/query_result.dart';
import 'src/strings.dart';
import 'src/generated/classes.dart';

import 'paper_form.dart';
import 'paper_view.dart';

////////////////////////////////////////////////////////////////////////////////

class PaperScreen extends StatefulWidget {
  @override
  State<PaperScreen> createState() => _PaperState();
}

////////////////////////////////////////////////////////////////////////////////

class _PaperState extends State<PaperScreen> {
  @override
  Widget build(final BuildContext context) {
    return EntityScreen(
      title: Strings.of(context).papers,
      subclasses: EntityClasses.all['paper'].subclasses,
      tabs: {
        Strings.of(context).starred: PapersStarredTab(),
        Strings.of(context).unread:  PapersUnreadTab(),
        Strings.of(context).read:  PapersReadTab(),
      },
      onPressCreate: () {
        Navigator.push(context, MaterialPageRoute<void>(
          builder: (final BuildContext context) => PaperForm(),
          fullscreenDialog: true,
        ));
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class PapersStarredTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return QueryBuilder(
      future: database.findPapers(starred: true),
      onSuccess: (final BuildContext context, final QueryResult result) {
        return PaperView(result);
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class PapersReadTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return QueryBuilder(
      future: database.findPapers(read: true),
      onSuccess: (final BuildContext context, final QueryResult result) {
        return PaperView(result);
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class PapersUnreadTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return QueryBuilder(
      future: database.findPapers(read: false),
      onSuccess: (final BuildContext context, final QueryResult result) {
        return PaperView(result);
      },
    );
  }
}
