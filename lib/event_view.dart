/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';

import 'src/query_result.dart';

////////////////////////////////////////////////////////////////////////////////

class EventView extends StatelessWidget {
  final QueryResult result;

  EventView(this.result, {Key key}) : super(key: key);

  @override
  Widget build(final BuildContext context) {
    return ListView.separated(
      padding: EdgeInsets.all(8.0),
      itemCount: result.length,
      itemBuilder: (final BuildContext context, final int index) {
        var row = result.rows[index];
        return ListTile(
          //leading: CircleAvatar(child: Text(row['title'].substring(0, 1))),
          leading: Icon(Icons.cake), // TODO
          title: Text(row['date']),
          subtitle: Text(row['title']),
          trailing: Icon(Icons.info, color: Theme.of(context).disabledColor),
        );
      },
      separatorBuilder: (final BuildContext context, final int index) {
        return Divider();
      },
    );
  }
}
