/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';

import 'src/database.dart';
import 'src/entity_screen.dart';
import 'src/query_builder.dart';
import 'src/query_result.dart';
import 'src/strings.dart';
import 'src/generated/classes.dart';

import 'event_form.dart';
import 'event_view.dart';

////////////////////////////////////////////////////////////////////////////////

class EventScreen extends StatefulWidget {
  @override
  State<EventScreen> createState() => _EventState();
}

////////////////////////////////////////////////////////////////////////////////

class _EventState extends State<EventScreen> {
  @override
  Widget build(final BuildContext context) {
    return EntityScreen(
      title: Strings.of(context).events,
      subclasses: EntityClasses.all['event'].subclasses,
      tabs: {
        Strings.of(context).upcoming: EventUpcomingTab(),
        Strings.of(context).recent:   EventRecentTab(),
      },
      onPressCreate: () {
        Navigator.push(context, MaterialPageRoute<void>(
          builder: (final BuildContext context) => EventForm(),
          fullscreenDialog: true,
        ));
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class EventRecentTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return QueryBuilder(
      future: database.findEventsBeforeNow(),
      onSuccess: (final BuildContext context, final QueryResult result) {
        return EventView(result);
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class EventUpcomingTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return QueryBuilder(
      future: database.findEventsAfterNow(),
      onSuccess: (final BuildContext context, final QueryResult result) {
        return EventView(result);
      },
    );
  }
}
