/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';

import 'src/query_result.dart';

////////////////////////////////////////////////////////////////////////////////

class PersonView extends StatelessWidget {
  final QueryResult result;

  PersonView(this.result, {Key key}) : super(key: key);

  @override
  Widget build(final BuildContext context) {
    return ListView.separated(
      padding: EdgeInsets.all(8.0),
      itemCount: result.length,
      itemBuilder: (final BuildContext context, final int index) {
        var row = result.rows[index];
        final String primaryName = row['name'] ?? row['name_uk'] ?? row['name_ru'] ?? row['nickname'] ?? "";
        final String secondaryName = row['name_uk'] ?? row['name_ru'] ?? ""; // TODO
        return ListTile(
          leading: CircleAvatar(child: Text(primaryName.isNotEmpty ? primaryName.substring(0, 1) : "N/A")),
          title: Text(primaryName),
          subtitle: Text(secondaryName),
          trailing: Icon(Icons.info, color: Theme.of(context).disabledColor),
        );
      },
      separatorBuilder: (final BuildContext context, final int index) {
        return Divider();
      },
    );
  }
}
