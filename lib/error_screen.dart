/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';

import 'src/error_report.dart';
import 'src/strings.dart';

////////////////////////////////////////////////////////////////////////////////

class ErrorScreen extends StatelessWidget {
  final Object error;

  ErrorScreen({Key key, this.error}) : super(key: key);

  @override
  Widget build(final BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Strings.of(context).error),
        actions: <Widget>[],
      ),
      body: ErrorReport(error: error),
    );
  }
}
