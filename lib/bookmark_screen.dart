/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';

import 'src/database.dart';
import 'src/entity_screen.dart';
import 'src/query_builder.dart';
import 'src/query_result.dart';
import 'src/strings.dart';
import 'src/generated/classes.dart';

import 'bookmark_form.dart';
import 'bookmark_view.dart';

////////////////////////////////////////////////////////////////////////////////

class BookmarkScreen extends StatefulWidget {
  @override
  State<BookmarkScreen> createState() => _BookmarkState();
}

////////////////////////////////////////////////////////////////////////////////

class _BookmarkState extends State<BookmarkScreen> {
  @override
  Widget build(final BuildContext context) {
    return EntityScreen(
      title: Strings.of(context).bookmarks,
      subclasses: EntityClasses.all['bookmark'].subclasses,
      tabs: {
        Strings.of(context).starred: BookmarkStarredTab(),
        Strings.of(context).all:     BookmarkAllTab(),
      },
      onPressCreate: () {
        Navigator.push(context, MaterialPageRoute<void>(
          builder: (final BuildContext context) => BookmarkForm(),
          fullscreenDialog: true,
        ));
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class BookmarkStarredTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return QueryBuilder(
      future: database.findBookmarks(starred: true),
      onSuccess: (final BuildContext context, final QueryResult result) {
        return BookmarkView(result);
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class BookmarkAllTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return QueryBuilder(
      future: database.findBookmarks(),
      onSuccess: (final BuildContext context, final QueryResult result) {
        return BookmarkView(result);
      },
    );
  }
}
