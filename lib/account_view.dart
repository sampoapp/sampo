/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';

import 'src/query_result.dart';

////////////////////////////////////////////////////////////////////////////////

class AccountView extends StatelessWidget {
  final QueryResult result;

  AccountView(this.result, {Key key}) : super(key: key);

  @override
  Widget build(final BuildContext context) {
    return ListView.separated(
      padding: EdgeInsets.all(8.0),
      itemCount: result.length,
      itemBuilder: (final BuildContext context, final int index) {
        var row = result.rows[index];
        return ListTile(
          leading: Icon(Icons.account_circle),
          title: Text(row['title']),
          subtitle: Text(row['url']),
          trailing: Icon(Icons.info, color: Theme.of(context).disabledColor),
        );
      },
      separatorBuilder: (final BuildContext context, final int index) {
        return Divider();
      },
    );
  }
}
