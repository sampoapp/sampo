/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'src/config.dart';
import 'src/database.dart';
import 'src/strings.dart';

import 'config_screen.dart';
import 'error_screen.dart';
import 'main_screen.dart';
import 'routes.dart';

////////////////////////////////////////////////////////////////////////////////

void main() => runApp(App());

////////////////////////////////////////////////////////////////////////////////

class App extends StatefulWidget {
  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  Object _error;

  final Map<String, WidgetBuilder> appRoutes = {
    ConfigScreen.route: (context) => ConfigScreen(),
  };

  @override
  void initState() {
    super.initState();
    _load();
  }

  @override
  void dispose() {
    database.close();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    final Map<String, WidgetBuilder> routes = {};
    routes.addAll(appRoutes);
    routes.addAll(listRoutes);
    return MaterialApp(
      onGenerateTitle: (final BuildContext context) => Strings.of(context).appTitle,
      localizationsDelegates: [
        StringsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: Strings.supportedLocales,
      color: Colors.grey,
      theme: ThemeData(
        primaryColor: Colors.black,
        brightness: Brightness.dark,
      ),
      home: (_error != null) ? ErrorScreen(error: _error) : MainScreen(),
      routes: routes,
    );
  }

  void _load() async {
    try {
      await config.init();
      await database.init();
    }
    catch (error) {
      setState(() {
        _error = error;
      });
    }
  }
}
