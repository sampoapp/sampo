/* This is free and unencumbered software released into the public domain. */

import 'dart:io';
import 'dart:typed_data' show Uint8List;

import 'package:archive/archive.dart';
import 'package:archive/archive_io.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'src/config.dart';
import 'src/database.dart';
import 'src/strings.dart';
import 'src/tab_label.dart';
import 'src/generated/classes.dart';

import 'config_dialogs.dart';

////////////////////////////////////////////////////////////////////////////////

class ConfigScreen extends StatefulWidget {
  static const route = "/config";

  @override
  State<ConfigScreen> createState() => _ConfigState();
}

////////////////////////////////////////////////////////////////////////////////

class _ConfigState extends State<ConfigScreen> {
  final List<Tab> _tabs = <Tab>[
    Tab(child: TabLabel((context) => Strings.of(context).database)),
    Tab(child: TabLabel((context) => Strings.of(context).users)),
    Tab(child: TabLabel((context) => Strings.of(context).servers)),
    Tab(child: TabLabel((context) => Strings.of(context).entitites)),
    Tab(child: TabLabel((context) => Strings.of(context).credentials)),
  ];

  @override
  Widget build(final BuildContext context) {
    return DefaultTabController(
      length: _tabs.length,
      child: Scaffold(
        appBar: AppBar(
          title: Text(Strings.of(context).settings),
          actions: <Widget>[],
          bottom: TabBar(
            tabs: _tabs,
            isScrollable: true,
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            ConfigDatabaseTab(),
            ConfigUsersTab(),
            ConfigServersTab(),
            ConfigEntitiesTab(),
            ConfigCredentialsTab(),
          ],
        ),
      ),
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class ConfigDatabaseTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return ListView(
      padding: EdgeInsets.symmetric(vertical: 36.0, horizontal: 36.0),
      shrinkWrap: true,
      children: <Widget>[
        RaisedButton(
          child: Text(Strings.of(context).saveSnapshot.toUpperCase()),
          onPressed: null, // TODO: () {},
        ),
        RaisedButton(
          child: Text(Strings.of(context).loadSnapshot.toUpperCase()),
          onPressed: () async {
            final Uri snapshotURL = await LoadSnapshotDialog.show(context, config.getString('config.snapshotURL'));
            if (snapshotURL != null) {
              await config.setString('config.snapshotURL', snapshotURL?.toString());
              await _loadSnapshot(context, snapshotURL);
              _showMessage(context, "Cleared the database and loaded snapshot."); // FIXME
            }
          },
        ),
        RaisedButton(
          child: Text(Strings.of(context).loadDemo.toUpperCase()), // TODO: replace text
          onPressed: () async {
            final Uri snapshotZipURL = Uri.parse("https://github.com/sampoapp/sampo-demos/files/2727730/batman.db.zip");
            if (await _confirm(context, Strings.of(context).loadDemoQ, Strings.of(context).confirmResetDatabase)) {
              await _loadSnapshot(context, snapshotZipURL, isZipped: true);
              _showMessage(context, "Cleared the database and loaded Batman scenario demo data."); // FIXME
            }
          },
        ),
        RaisedButton(
          child: Text(Strings.of(context).loadDemo.toUpperCase()), // TODO: replace text
          onPressed: () async {
            final Uri snapshotZipURL = Uri.parse("https://github.com/sampoapp/sampo-demos/files/2727731/matrix.db.zip");
            if (await _confirm(context, Strings.of(context).loadDemoQ, Strings.of(context).confirmResetDatabase)) {
              await _loadSnapshot(context, snapshotZipURL, isZipped: true);
              _showMessage(context, "Cleared the database and loaded Matrix scenario demo data."); // FIXME
            }
          },
        ),
        RaisedButton(
          child: Text(Strings.of(context).clearDatabase.toUpperCase()),
          onPressed: () async {
            if (await _confirm(context, Strings.of(context).clearDatabaseQ, Strings.of(context).confirmResetDatabase)) {
              await database.clear();
              _showMessage(context, "Cleared the database."); // FIXME
            }
          },
        ),
      ].map((final Widget button) {
        // Add a little space between the buttons:
        return Container(
          padding: EdgeInsets.symmetric(vertical: 4.0),
          child: button,
        );
      }).toList(),
    );
  }

  Future<bool> _confirm(final BuildContext context, final String title, final String body) async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: true,
      builder: (final BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(
            child: Text(body),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(Strings.of(context).cancel.toUpperCase()),
              onPressed: () => Navigator.of(context).pop(false),
            ),
            FlatButton(
              child: Text(Strings.of(context).proceed.toUpperCase()),
              onPressed: () => Navigator.of(context).pop(true),
            ),
          ],
        );
      },
    );
  }

  Future<void> _loadSnapshot(final BuildContext context, final Uri snapshotURL, {final bool isZipped = false}) {
    assert(snapshotURL != null);
    switch (snapshotURL.scheme) {
      case 'file':  return _loadSnapshotFromFile(context, snapshotURL, isZipped: isZipped);
      case 'http':  // fall through
      case 'https': return _loadSnapshotFromHTTP(context, snapshotURL, isZipped: isZipped);
    }
  }

  Future<void> _loadSnapshotFromFile(final BuildContext context, final Uri fileURL, {final bool isZipped = false}) async {
    print("Loading snapshot from a local file: $fileURL"); // DEBUG
    try {
      await database.replaceWithFile(File.fromUri(fileURL));
    }
    on UnsupportedError catch (error) {
      _showMessage(context, error.toString());
    }
    on FileSystemException catch (error) {
      _showMessage(context, error.toString());
    }
  }

  Future<void> _loadSnapshotFromHTTP(final BuildContext context, final Uri httpURL, {final bool isZipped = false}) async {
    print("Loading snapshot from a HTTP(S) URL: $httpURL"); // DEBUG
    try {
      Uint8List data = await http.readBytes(httpURL);
      if (isZipped) { // TODO: catch errors
        final Archive archive = ZipDecoder().decodeBuffer(InputStream(data));
        data = archive.first.content as Uint8List;
      }
      await database.replaceWithData(data.buffer);
    }
    on SocketException catch (error) { // DNS lookup failed
      _showMessage(context, error.toString());
    }
    on http.ClientException catch (error) {
      _showMessage(context, error.toString());
    }
  }

  void _showMessage(final BuildContext context, final String message) {
    print(message); // DEBUG
    Scaffold.of(context).showSnackBar(
      SnackBar(
        content: Text(message, textAlign: TextAlign.center),
      ),
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class ConfigUsersTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return ListView.separated(
      padding: EdgeInsets.all(8.0),
      itemCount: 0,
      itemBuilder: (final BuildContext context, final int index) {
        return null; // TODO
      },
      separatorBuilder: (final BuildContext context, final int index) {
        return Divider();
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class ConfigServersTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return ListView.separated(
      padding: EdgeInsets.all(8.0),
      itemCount: 0,
      itemBuilder: (final BuildContext context, final int index) {
        return null; // TODO
      },
      separatorBuilder: (final BuildContext context, final int index) {
        return Divider();
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class ConfigEntitiesTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return ListView.separated(
      padding: EdgeInsets.all(8.0),
      itemCount: EntityClasses.top.length,
      itemBuilder: (final BuildContext context, final int index) {
        var modelID = EntityClasses.top[index];
        var model = EntityClasses.all[modelID];
        return SwitchListTile(
          title: Text(Strings.of(context).getSingular(model.id)),
          value: config.getBool("class.${model.id}") ?? true,
          secondary: Icon(model.icon),
          onChanged: (final bool value) async {
            await config.setBool("class.${model.id}", value);
          },
        );
      },
      separatorBuilder: (final BuildContext context, final int index) {
        return Divider();
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class ConfigCredentialsTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return ListView.separated(
      padding: EdgeInsets.all(8.0),
      itemCount: 0,
      itemBuilder: (final BuildContext context, final int index) {
        return null; // TODO
      },
      separatorBuilder: (final BuildContext context, final int index) {
        return Divider();
      },
    );
  }
}
