/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';

import 'src/database.dart';
import 'src/entity_screen.dart';
import 'src/query_builder.dart';
import 'src/query_result.dart';
import 'src/strings.dart';
import 'src/generated/classes.dart';

import 'account_form.dart';
import 'account_view.dart';

////////////////////////////////////////////////////////////////////////////////

class AccountScreen extends StatefulWidget {
  @override
  State<AccountScreen> createState() => _AccountState();
}

////////////////////////////////////////////////////////////////////////////////

class _AccountState extends State<AccountScreen> {
  @override
  Widget build(final BuildContext context) {
    return EntityScreen(
      title: Strings.of(context).accounts,
      subclasses: EntityClasses.all['account'].subclasses,
      tabs: {
        Strings.of(context).starred: AccountStarredTab(),
        Strings.of(context).all:     AccountAllTab(),
      },
      onPressCreate: () {
        Navigator.push(context, MaterialPageRoute<void>(
          builder: (final BuildContext context) => AccountForm(),
          fullscreenDialog: true,
        ));
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class AccountStarredTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return QueryBuilder(
      future: database.findAccounts(starred: true),
      onSuccess: (final BuildContext context, final QueryResult result) {
        return AccountView(result);
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class AccountAllTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return QueryBuilder(
      future: database.findAccounts(),
      onSuccess: (final BuildContext context, final QueryResult result) {
        return AccountView(result);
      },
    );
  }
}
