/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';

import 'src/database.dart';
import 'src/entity_screen.dart';
import 'src/query_builder.dart';
import 'src/query_result.dart';
import 'src/strings.dart';
import 'src/generated/classes.dart';

import 'person_form.dart';
import 'person_view.dart';

////////////////////////////////////////////////////////////////////////////////

class PersonScreen extends StatefulWidget {
  @override
  State<PersonScreen> createState() => _PersonState();
}

////////////////////////////////////////////////////////////////////////////////

class _PersonState extends State<PersonScreen> {
  @override
  Widget build(final BuildContext context) {
    return EntityScreen(
      title: Strings.of(context).people,
      subclasses: EntityClasses.all['person'].subclasses,
      tabs: {
        Strings.of(context).starred: PersonStarredTab(),
        Strings.of(context).all:     PersonAllTab(),
      },
      onPressCreate: () {
        Navigator.push(context, MaterialPageRoute<void>(
          builder: (final BuildContext context) => PersonForm(),
          fullscreenDialog: true,
        ));
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class PersonStarredTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return QueryBuilder(
      future: database.findPeople(starred: true),
      onSuccess: (final BuildContext context, final QueryResult result) {
        return PersonView(result);
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class PersonAllTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return QueryBuilder(
      future: database.findPeople(),
      onSuccess: (final BuildContext context, final QueryResult result) {
        return PersonView(result);
      },
    );
  }
}
