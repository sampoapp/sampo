/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart' show WidgetBuilder;

import 'account_screen.dart';
import 'bookmark_screen.dart';
import 'contract_screen.dart';
import 'document_screen.dart';
import 'event_screen.dart';
import 'network_screen.dart';
import 'note_screen.dart';
import 'paper_screen.dart';
import 'payment_screen.dart';
import 'person_screen.dart';
import 'place_screen.dart';
import 'project_screen.dart';
import 'quote_screen.dart';
import 'task_screen.dart';

////////////////////////////////////////////////////////////////////////////////

Map<String, WidgetBuilder> listRoutes = {
  '/list/account': (context) => AccountScreen(),
  '/list/bookmark': (context) => BookmarkScreen(),
  '/list/contract': (context) => ContractScreen(),
  '/list/document': (context) => DocumentScreen(),
  '/list/event': (context) => EventScreen(),
  //'/list/network': (context) => NetworkScreen(),
  '/list/note': (context) => NoteScreen(),
  '/list/paper': (context) => PaperScreen(),
  '/list/payment': (context) => PaymentScreen(),
  '/list/person': (context) => PersonScreen(),
  '/list/place': (context) => PlaceScreen(),
  '/list/project': (context) => ProjectScreen(),
  '/list/quote': (context) => QuoteScreen(),
  //'/list/task': (context) => TaskScreen(),
};
