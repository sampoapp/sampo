/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';

import 'src/query_result.dart';

////////////////////////////////////////////////////////////////////////////////

class QuoteView extends StatelessWidget {
  final QueryResult result;

  QuoteView(this.result, {Key key}) : super(key: key);

  @override
  Widget build(final BuildContext context) {
    return ListView.separated(
      padding: EdgeInsets.all(8.0),
      itemCount: result.length,
      itemBuilder: (final BuildContext context, final int index) {
        var row = result.rows[index];
        return ExpansionTile(
          title: Text(row['body'], overflow: TextOverflow.ellipsis),
          backgroundColor: Theme.of(context).accentColor.withOpacity(0.025),
          children: <Widget>[
            ListTile(
              leading: CircleAvatar(child: Text(_makeInitials(row['author']))),
              title: Text(row['body']),
              subtitle: Text(row['author']),
            ),
            Container(padding: EdgeInsets.only(bottom: 16.0)),
          ],
        );
      },
      separatorBuilder: (final BuildContext context, final int index) {
        return Divider();
      },
    );
  }

  String _makeInitials(final String fullName) {
    return fullName.split(" ").take(2).map((word) => word[0]).join(" ");
  }
}
