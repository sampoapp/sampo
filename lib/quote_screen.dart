/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';

import 'src/database.dart';
import 'src/entity_screen.dart';
import 'src/query_builder.dart';
import 'src/query_result.dart';
import 'src/strings.dart';
import 'src/generated/classes.dart';

import 'quote_form.dart';
import 'quote_view.dart';

////////////////////////////////////////////////////////////////////////////////

class QuoteScreen extends StatefulWidget {
  @override
  State<QuoteScreen> createState() => _QuoteState();
}

////////////////////////////////////////////////////////////////////////////////

class _QuoteState extends State<QuoteScreen> {
  @override
  Widget build(final BuildContext context) {
    return EntityScreen(
      title: Strings.of(context).quotes,
      subclasses: EntityClasses.all['quote'].subclasses,
      tabs: {
        Strings.of(context).starred: QuotesStarredTab(),
        Strings.of(context).all:     QuotesAllTab(),
      },
      onPressCreate: () {
        Navigator.push(context, MaterialPageRoute<void>(
          builder: (final BuildContext context) => QuoteForm(),
          fullscreenDialog: true,
        ));
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class QuotesStarredTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return QueryBuilder(
      future: database.findQuotes(starred: true),
      onSuccess: (final BuildContext context, final QueryResult result) {
        return QuoteView(result);
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class QuotesAllTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return QueryBuilder(
      future: database.findQuotes(),
      onSuccess: (final BuildContext context, final QueryResult result) {
        return QuoteView(result);
      },
    );
  }
}
