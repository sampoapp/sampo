/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';

import 'src/database.dart';
import 'src/entity_screen.dart';
import 'src/query_builder.dart';
import 'src/query_result.dart';
import 'src/strings.dart';
import 'src/tab_placeholder.dart';
import 'src/generated/classes.dart';

import 'project_form.dart';

////////////////////////////////////////////////////////////////////////////////

class ProjectScreen extends StatefulWidget {
  @override
  State<ProjectScreen> createState() => _ProjectState();
}

////////////////////////////////////////////////////////////////////////////////

class _ProjectState extends State<ProjectScreen> {
  @override
  Widget build(final BuildContext context) {
    return EntityScreen(
      title: Strings.of(context).projects,
      subclasses: EntityClasses.all['project'].subclasses,
      tabs: {
        Strings.of(context).active:   ProjectsActiveTab(),
        Strings.of(context).inactive: ProjectsInactiveTab(),
        Strings.of(context).archived: ProjectsArchivedTab(),
      },
      onPressCreate: () {
        Navigator.push(context, MaterialPageRoute<void>(
          builder: (final BuildContext context) => ProjectForm(),
          fullscreenDialog: true,
        ));
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class ProjectsActiveTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return QueryBuilder(
      future: database.findProjects(), // TODO
      onSuccess: (final BuildContext context, final QueryResult result) {
        return ListView.builder(
          padding: EdgeInsets.all(8.0),
          itemCount: result.length,
          itemBuilder: (final BuildContext context, final int index) {
            var row = result.rows[index];
            return ListTile(
              title: Text("${row['title']}"), // TODO
            );
          },
        );
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class ProjectsArchivedTab extends TabPlaceholder {
  ProjectsArchivedTab({Key key}) : super(title: "Archived projects", key: key); // TODO
}

////////////////////////////////////////////////////////////////////////////////

class ProjectsInactiveTab extends TabPlaceholder {
  ProjectsInactiveTab({Key key}) : super(title: "Inactive projects", key: key); // TODO
}
