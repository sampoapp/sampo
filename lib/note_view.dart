/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';

import 'src/query_result.dart';

////////////////////////////////////////////////////////////////////////////////

class NoteView extends StatelessWidget {
  final QueryResult result;

  NoteView(this.result, {Key key}) : super(key: key);

  @override
  Widget build(final BuildContext context) {
    return ListView.separated(
      padding: EdgeInsets.all(8.0),
      itemCount: result.length,
      itemBuilder: (final BuildContext context, final int index) {
        var row = result.rows[index];
        return ExpansionTile(
          title: Text(row['body'], overflow: TextOverflow.ellipsis),
          backgroundColor: Theme.of(context).accentColor.withOpacity(0.025),
          children: <Widget>[
            ListTile(
              title: Text(row['body']),
              subtitle: Text("${row['created_at'] ?? ''}"), // TODO
            ),
            Container(padding: EdgeInsets.only(bottom: 16.0)),
          ],
        );
      },
      separatorBuilder: (final BuildContext context, final int index) {
        return Divider();
      },
    );
  }
}
