/* This is free and unencumbered software released into the public domain. */

import 'package:flutter/material.dart';

import 'src/database.dart';
import 'src/entity_screen.dart';
import 'src/query_builder.dart';
import 'src/query_result.dart';
import 'src/strings.dart';
import 'src/generated/classes.dart';

import 'document_form.dart';
import 'document_view.dart';

////////////////////////////////////////////////////////////////////////////////

class DocumentScreen extends StatefulWidget {
  @override
  State<DocumentScreen> createState() => _DocumentState();
}

////////////////////////////////////////////////////////////////////////////////

class _DocumentState extends State<DocumentScreen> {
  @override
  Widget build(final BuildContext context) {
    return EntityScreen(
      title: Strings.of(context).documents,
      subclasses: EntityClasses.all['document'].subclasses,
      tabs: {
        Strings.of(context).starred: DocumentStarredTab(),
        Strings.of(context).all:     DocumentAllTab(),
      },
      onPressCreate: () {
        Navigator.push(context, MaterialPageRoute<void>(
          builder: (final BuildContext context) => DocumentForm(),
          fullscreenDialog: true,
        ));
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class DocumentStarredTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return QueryBuilder(
      future: database.findDocuments(starred: true),
      onSuccess: (final BuildContext context, final QueryResult result) {
        return DocumentView(result);
      },
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class DocumentAllTab extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return QueryBuilder(
      future: database.findDocuments(),
      onSuccess: (final BuildContext context, final QueryResult result) {
        return DocumentView(result);
      },
    );
  }
}
