DROP TABLE IF EXISTS user;
CREATE TABLE user (
  id INTEGER PRIMARY KEY NOT NULL,
  uuid CHAR(36) UNIQUE NOT NULL, -- FIXME
  nick TEXT NULL,
  name TEXT NULL
);

DROP TABLE IF EXISTS log;
CREATE TABLE log (
  id INTEGER PRIMARY KEY NOT NULL,
  updated_by INTEGER NULL REFERENCES user,
  updated_at INTEGER NULL
  -- TODO
);

--------------------------------------------------------------------------------

DROP TABLE IF EXISTS data;
CREATE TABLE data (
  id INTEGER PRIMARY KEY NOT NULL,
  uuid CHAR(36) UNIQUE NOT NULL, -- FIXME
  created_by INTEGER NOT NULL REFERENCES user,
  created_at INTEGER NOT NULL,
  updated_by INTEGER NULL REFERENCES user,
  updated_at INTEGER NULL
);

--------------------------------------------------------------------------------

DROP TABLE IF EXISTS data_account;
CREATE TABLE data_account (
  id INTEGER PRIMARY KEY NOT NULL,
  title TEXT NOT NULL,
  url TEXT NULL
);

DROP TABLE IF EXISTS data_account_bank_account;
CREATE TABLE data_account_bank_account (id INTEGER PRIMARY KEY NOT NULL);

--------------------------------------------------------------------------------

DROP TABLE IF EXISTS data_bookmark;
CREATE TABLE data_bookmark (
  id INTEGER PRIMARY KEY NOT NULL,
  url TEXT NOT NULL,
  title TEXT NULL
);

DROP TABLE IF EXISTS data_bookmark_amazon_product;
CREATE TABLE data_bookmark_amazon_product (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_arto_note;
CREATE TABLE data_bookmark_arto_note (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_arxiv_article;
CREATE TABLE data_bookmark_arxiv_article (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_bitbucket_repository;
CREATE TABLE data_bookmark_bitbucket_repository (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_coursera_course;
CREATE TABLE data_bookmark_coursera_course (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_dart_package;
CREATE TABLE data_bookmark_dart_package (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_facebook_page;
CREATE TABLE data_bookmark_facebook_page (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_github_account;
CREATE TABLE data_bookmark_github_account (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_github_repository;
CREATE TABLE data_bookmark_github_repository (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_google_document;
CREATE TABLE data_bookmark_google_document (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_google_group;
CREATE TABLE data_bookmark_google_group (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_google_play_app;
CREATE TABLE data_bookmark_google_play_app (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_google_search;
CREATE TABLE data_bookmark_google_search (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_hex_package;
CREATE TABLE data_bookmark_hex_package (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_ietf_rfc;
CREATE TABLE data_bookmark_ietf_rfc (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_meetup_group;
CREATE TABLE data_bookmark_meetup_group (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_python_package;
CREATE TABLE data_bookmark_python_package (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_reddit_thread;
CREATE TABLE data_bookmark_reddit_thread (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_ruby_gem;
CREATE TABLE data_bookmark_ruby_gem (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_twitter_tweet;
CREATE TABLE data_bookmark_twitter_tweet (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_vimeo_video;
CREATE TABLE data_bookmark_vimeo_video (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_website;
CREATE TABLE data_bookmark_website (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_wikipedia_article;
CREATE TABLE data_bookmark_wikipedia_article (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_wikipedia_category;
CREATE TABLE data_bookmark_wikipedia_category (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_wikiquote_page;
CREATE TABLE data_bookmark_wikiquote_page (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_wiktionary_entry;
CREATE TABLE data_bookmark_wiktionary_entry (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_youtube_channel;
CREATE TABLE data_bookmark_youtube_channel (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_youtube_playlist;
CREATE TABLE data_bookmark_youtube_playlist (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_bookmark_youtube_video;
CREATE TABLE data_bookmark_youtube_video (id INTEGER PRIMARY KEY NOT NULL);

--------------------------------------------------------------------------------

DROP TABLE IF EXISTS data_contract;
CREATE TABLE data_contract (
  id INTEGER PRIMARY KEY NOT NULL,
  date CHAR(10) NOT NULL,
  title TEXT NOT NULL
);

--------------------------------------------------------------------------------

DROP TABLE IF EXISTS data_document;
CREATE TABLE data_document (
  id INTEGER PRIMARY KEY NOT NULL
);

DROP TABLE IF EXISTS data_document_email;
CREATE TABLE data_document_email (id INTEGER PRIMARY KEY NOT NULL);

--------------------------------------------------------------------------------

DROP TABLE IF EXISTS data_event;
CREATE TABLE data_event (
  id INTEGER PRIMARY KEY NOT NULL,
  date CHAR(10) NOT NULL,
  title TEXT NOT NULL,
  url TEXT NULL
);

DROP TABLE IF EXISTS data_event_birthday;
CREATE TABLE data_event_birthday (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_event_holiday;
CREATE TABLE data_event_holiday (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_event_meeting;
CREATE TABLE data_event_meeting (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_event_meetup;
CREATE TABLE data_event_meetup (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_event_party;
CREATE TABLE data_event_party (id INTEGER PRIMARY KEY NOT NULL);

--------------------------------------------------------------------------------

DROP TABLE IF EXISTS data_network;
CREATE TABLE data_network (
  id INTEGER PRIMARY KEY NOT NULL,
  title TEXT NOT NULL
);

DROP TABLE IF EXISTS data_network_wired_network;
CREATE TABLE data_network_wired_network (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_network_wireless_network;
CREATE TABLE data_network_wireless_network (id INTEGER PRIMARY KEY NOT NULL);

--------------------------------------------------------------------------------

DROP TABLE IF EXISTS data_note;
CREATE TABLE data_note (
  id INTEGER PRIMARY KEY NOT NULL,
  body TEXT NOT NULL
);

--------------------------------------------------------------------------------

DROP TABLE IF EXISTS data_paper;
CREATE TABLE data_paper (
  id INTEGER PRIMARY KEY NOT NULL,
  authors TEXT NULL,
  title TEXT NOT NULL,
  abstract TEXT NULL
);

--------------------------------------------------------------------------------

DROP TABLE IF EXISTS data_payment;
CREATE TABLE data_payment (
  id INTEGER PRIMARY KEY NOT NULL,
  due_at CHAR(10) NULL,
  amount REAL NOT NULL,
  currency CHAR(3) NOT NULL,
  title TEXT NULL
);

--------------------------------------------------------------------------------

DROP TABLE IF EXISTS data_person;
CREATE TABLE data_person (
  id INTEGER PRIMARY KEY NOT NULL,
  nickname TEXT NULL,
  name TEXT NULL,            -- Common name in Latin script
  name_ru TEXT NULL,         -- Common name in Russian script
  name_uk TEXT NULL,         -- Common name in Ukrainian script
  formal_name TEXT NULL,     -- Formal name in Latin script
  formal_name_ru TEXT NULL,  -- Formal name in Russian script
  formal_name_uk TEXT NULL,  -- Formal name in Ukrainian script
  birthday CHAR(10) NULL,
  twitter_nick TEXT NULL,
  facebook_nick TEXT NULL
);

DROP TABLE IF EXISTS data_person_man;
CREATE TABLE data_person_man (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_person_woman;
CREATE TABLE data_person_woman (id INTEGER PRIMARY KEY NOT NULL);

--------------------------------------------------------------------------------

DROP TABLE IF EXISTS data_place;
CREATE TABLE data_place (
  id INTEGER PRIMARY KEY NOT NULL,
  latitude REAL NOT NULL,  -- angle (0° to 90°)
  longitude REAL NOT NULL, -- angle (-180° to 180°)
  altitude REAL NULL,  -- meters above mean sea level (MAMSL)
  title TEXT NULL
);

DROP TABLE IF EXISTS data_place_building;
CREATE TABLE data_place_building (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_place_monument;
CREATE TABLE data_place_monument (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_place_restaurant;
CREATE TABLE data_place_restaurant (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_place_venue;
CREATE TABLE data_place_venue (id INTEGER PRIMARY KEY NOT NULL);

--------------------------------------------------------------------------------

DROP TABLE IF EXISTS data_project;
CREATE TABLE data_project (
  id INTEGER PRIMARY KEY NOT NULL,
  title TEXT NOT NULL
);

--------------------------------------------------------------------------------

DROP TABLE IF EXISTS data_quote;
CREATE TABLE data_quote (
  id INTEGER PRIMARY KEY NOT NULL,
  author TEXT NULL,
  body TEXT NOT NULL
);

--------------------------------------------------------------------------------

DROP TABLE IF EXISTS data_task;
CREATE TABLE data_task (
  id INTEGER PRIMARY KEY NOT NULL,
  title TEXT NOT NULL
);

DROP TABLE IF EXISTS data_task_assignment;
CREATE TABLE data_task_assignment (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_task_bounty;
CREATE TABLE data_task_bounty (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_task_chore;
CREATE TABLE data_task_chore (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_task_errand;
CREATE TABLE data_task_errand (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_task_promise;
CREATE TABLE data_task_promise (id INTEGER PRIMARY KEY NOT NULL);

DROP TABLE IF EXISTS data_task_purchase;
CREATE TABLE data_task_purchase (id INTEGER PRIMARY KEY NOT NULL);
