******
To-Dos
******

Security
========

- Prevent screenshots of user data by setting `FLAG_SECURE
  <https://developer.android.com/reference/android/view/WindowManager.LayoutParams.html#FLAG_SECURE>`__
