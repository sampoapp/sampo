/* This is free and unencumbered software released into the public domain. */

package org.sampoapp.android;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import io.flutter.app.FlutterActivity;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {
  private static final String TAG = "Sampo";

  private String sharedText;

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    GeneratedPluginRegistrant.registerWith(this);

    Log.i(TAG, "onCreate");

    final Intent intent = getIntent();
    final String action = intent.getAction();
    final String type = intent.getType();

    if (Intent.ACTION_SEND.equals(action) && type != null) {
      if ("text/plain".equals(type)) {
        this.handleSendText(intent);
        Log.i(TAG, "onCreate: android.intent.action.SEND EXTRA_TEXT=" + this.sharedText);
      }
    }

    // TODO: https://flutter.io/flutter-for-android/#how-do-i-handle-incoming-intents-from-external-applications-in-flutter
  }

  void handleSendText(final Intent intent) {
    this.sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
  }
}
